FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > evince.log'

COPY evince .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' evince
RUN bash ./docker.sh

RUN rm --force --recursive evince
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD evince
